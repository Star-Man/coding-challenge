#!/bin/sh

# Exit on failure to prevent further errors
set -e

checkGPIO() {

	echo "Testing GPIO $1"

	# Check if already exported, if not then export
	if [ ! -d /sys/class/gpio/gpio"$1" ]; then
	    echo "$1" > /sys/class/gpio/export || echo "An error occured while exporting GPIO $1"
	fi

	# Set to input
	echo in > /sys/class/gpio/gpio"$1"/direction
	
	# Read the input
	inputValue=$(cat /sys/class/gpio/gpio"$1"/value)
	echo "Input value of GPIO $1 is $inputValue"

	# Set to output
	echo out > /sys/class/gpio/gpio"$1"/direction

	# Set to high
	echo 1 > /sys/class/gpio/gpio"$1"/value

	# Assuming there's a LED connected it would have turned on
	sleep 5

	# Set to low, LED turns off
	echo 0 > /sys/class/gpio/gpio"$1"/value
	sleep 5
}

# Loop checks 10 GPIOs
for i in {1..10}; do
	checkGPIO $i
done
echo "Test Passed"
