#!/bin/sh

# Exit on failure to prevent further errors
set -e

# Generate random file
dd if=/dev/urandom of=testFile bs=1000 count=8

# Save a hexdump of the random file
hexdump testFile >> testFileHexDump

# Check if already exported, if not then export
if [ ! -d /sys/class/gpio/gpio11 ]; then
    echo 11 > /sys/class/gpio/export || echo "An error occured while exporting GPIO 11"
fi

# Set to output
echo out > /sys/class/gpio/gpio11/direction

# Set to high to allow WRITE_ENABLE for EEPROM
echo 1 > /sys/class/gpio/gpio11/value

# Set test file data to EEPROM
cat testFile > /sys/class/spi_master/spi1/spi1.o/eeprom

# Set to low to disallow WRITE_ENABLE for EEPROM
echo 0 > /sys/class/gpio/gpio11/value

# Read the EEPROM
cat /sys/class/spi_master/spi1/spi1.o/eeprom >> eepromOutputFile

# Save a hexdump of the EEPROM output as a variable
hexdump eepromOutputFile >> eepromOutputHexDump

# Compare the original file and read data
cmp testFileHexDump eepromOutputHexDump
