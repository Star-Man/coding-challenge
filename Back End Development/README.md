# Back End Development

This folder contains a node module that is capable of running two seperate loops at the
same time. Before it does this it creates a database and loads it with data. The
script for setting up the database is called "setupDatabase.js". For the
purpose of this exercise, the database is dropped and rebuilt every time the script
is run. Three tables are created:

1. dsus, which is simply a list of dsus with their description
2. site_power, which measures the most recent power reading at a site
3. dsu_power, which shows the combined power readings of multiple sites per dsu

The dsus table is populated with 13 DSU values.

Then the dsu_power table is filled with 13 readings of 0 so they can be updated later.

Finally 100 sites are inserted, each site is given a randomised DSU value.

## Set Power Loop

A random id is selected from the 100 sites, and then a random power value is generated
between 0 and 1000. The power value of the selected site id is updated (along with the
current time) in the database.

## Aggregate Power Loop

This function loops through all 13 DSUs and gets the sum of all the power at every site
for that DSU. This is done with the sum(power) MySQL function. If the sum(power)
returns undefined (No sites have been measured yet) the dsu_power table is update for
that DSU with a total power level of 0. Otherwise the total power level is updated to
whatever was measured.

## Running

Just run <code>npm install</code> to make sure you have the required modules
Then run <code>npm start</code>.

. All setup is done for you

## Testing

There is a test script called setupDatabaseTest.js in the test
folder that runs a mocha test which does the entire database setup
and then validates that the correct data was created by querying in
MySQL.

Run this with <code>npm test</code>.

Unit tests couldn't be created in a useful way for the loops due to
their random nature.

## Assumptions
1. User has a MySQL/MariaDB server running
2. User has NodeJS
3. The "connectToDatabase.js" script depends on the environment variables MYSQLUSER and
MYSQLPASSWORD existing and being the correct values for accessing MySQL as a root user.
(I wouldn't normally use a root MySql user but dropping the DB is part of the script)
