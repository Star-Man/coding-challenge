# Embedded Device Development

This folder contains two bash scripts.

## testEEPROM.sh
This bash script attempts to create a random 8KB file and export its
contents to a EEPROM device which has already been loaded in the
kernel. It also sets the GPIO11 output to high to allow write access
to the EEPROM.

It then reads the EEPROM and saves the output to a file. The original
file and the output are compared for differences. If there's any
problems along the way the entire script will exit for safety.

The script requires no arguments and can be run as is.


## checkGPIO.sh
This bash script attempts to test GPIO pins 1 to 10. It does this by
looping through every pin and reading the input value, then setting
the pin to high and low outputs.

The script checks if the GPIO pin has already been exported in sysfs.
If there's any problems along the way the entire script will exit for safety.

The script requires no arguments and can be run as is.


## Assumptions
1. User is root
2. User has standard linux tools (dd, hexdump, cmp)
3. EEPROM device address is at /sys/class/spi_master/spi1/spi1.o/eeprom
