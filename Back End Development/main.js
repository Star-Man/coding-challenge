const { setIntervalAsync } = require('set-interval-async/dynamic')
const { createDatabaseConnection } = require('./connectToDatabase')
const { setupDatabase } = require('./setupDatabase')

async function setPower (con) {
  const id = Math.floor(Math.random() * Math.floor(100))
  const power = (Math.random() * 1000 + 1).toFixed(4)
  // Convert current time to a MySql DATETIME format
  const time = new Date().toISOString().slice(0, 19).replace('T', ' ')
  console.log(`Setting power level to ${power} for site ${id}`)
  return con.query(`UPDATE site_power SET power='${power}', time_sent='${time}' WHERE id='${id}'`)
}

async function aggregatePower (con) {
  // Convert current time to a MySql DATETIME format
  const time = new Date().toISOString().slice(0, 19).replace('T', ' ')
  const [dsus] = await con.query('SELECT * FROM dsus')
  for (const dsu of dsus) {
    const [powerLevel] = await con.query(`SELECT sum(power) FROM site_power WHERE dsu_id=${dsu.id}`)
    if (powerLevel[0]['sum(power)'] === null || powerLevel[0]['sum(power)'] === undefined) {
      con.query(`UPDATE dsu_power SET total_power='0', time_aggregated='${time}' WHERE dsu_id='${dsu.id}'`)
    } else {
      con.query(`UPDATE dsu_power SET total_power='${powerLevel[0]['sum(power)']}', time_aggregated='${time}' WHERE dsu_id='${dsu.id}'`)
    }
  }
}

async function main () {
  const con = await createDatabaseConnection()
  await setupDatabase(con)
  console.log('Done setting up DB')

  setIntervalAsync(() => setPower(con), 1000)
  setIntervalAsync(() => aggregatePower(con), 1000)
}

main()
