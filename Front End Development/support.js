// Handle user leaving with unsaved data
window.onbeforeunload = function () {
  if ($('.putInput').filter(function () { return $(this).val() }).length > 0) {
    return 'Are you sure you want to leave with unsaved data?'
  }
}

// This is the Div to place all new DSU's after, it will update top the lowest DSU
var previousDiv = '#searchBar'

function createDsuDiv (dsu, sitesDiv) {
  // Create a div for a newly created DSU to attach to the list of DSUs
  const dsuDiv = `
        <div id="dsu_${dsu.id}" title="${dsu.id}" class="dsuDiv">
            <div></div>
            <div class="dsuNameDiv dsuSection">
                <span class="dsuLabel">DSU Name</span>
                <span class="dsuNameValue">${dsu.name}</span>
            </div>
            <div class="dsuDescriptionDiv dsuSection">
                <span class="dsuLabel">DSU Description</span>
                <span class="dsuDescriptionValue">${dsu.description}</span>
            </div>
            <div class="dsuCertDiv dsuSection">
                <span class="dsuLabel">DSU Cert</span>
                <span class="dsuCertValue">${dsu.cert}</span>
            </div>
            <div></div>
            <div class="sitesDiv">
                <div></div>
                <div></div>
                <div class="sitesContent">
                    <table id="sitesTable_${dsu.id}" class="tablesorter">
                        <thead>
                            <tr>
                            <th>Site ID</th>
                            <th>Site Name</th>
                            <th>Site Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            ${sitesDiv}
                        </tbody>
                    </table>
                    <span class="addNewSpan" >Add New Site</span>
                    <div class="addNewDiv">
                        <input class="putInput" type="text" maxlength="10" placeholder="Site Name"></input>
                        <input class="putInput" type="text" maxlength="20" placeholder="Site Description"></input>
                        <span class="putInput tickSelector">✓</span>
                    </div>
                </div>
            </div> 
            <div class="expandDiv">
                <span class="expandSpan">▼</span>
            </div>
        </div>
    `
  return dsuDiv
}

function getDsus (siteResult) {
  // Get the list of DSUS
  var getDsusRequest = $.get('http://127.0.0.1:3000/v1/api/dsus')
  getDsusRequest.done(function (result) {
    $('#noApiLoaded').hide()

    for (const key in result) {
      const dsu = result[key]

      // Create a div that represents the total sites
      var sitesDiv = ''
      for (const key in siteResult) {
        const site = siteResult[key]
        // Add the table only to the correct DSU
        if (site.dsuId.toString() === dsu.id.toString()) {
          sitesDiv = sitesDiv + `
                    <tr>
                        <td>${site.id}</td>
                        <td>${site.name}</td>
                        <td>${site.description}</td>
                    </tr>
                `
        }
      }
      const dsuDiv = createDsuDiv(dsu, sitesDiv)
      $(dsuDiv).insertAfter(previousDiv)
      previousDiv = `#dsu_${dsu.id}`

      // Activate tablesorter
      $(`#sitesTable_${dsu.id}`).tablesorter({ theme: 'default' })
    }

    // Create the Add DSU button
    $(`
            <div class="dsuDiv" id="addDsuOuterDiv">
                <span class="addNewSpan" id="addDsuSpan" >Add New DSU</span>
                <div class="addNewDiv" id="addDsuDiv">
                    <input class="putInput" type="text" maxlength="10" placeholder="DSU Name"></input>
                    <input class="putInput" type="text" maxlength="20" placeholder="DSU Description"></input>
                    <input class="putInput" type="text" maxlength="20" placeholder="DSU Cert"></input>
                    <span class="putInput" id="dsuSelector">✓</span>
                </div>
            </div>
        `).insertAfter(previousDiv)
  })
  getDsusRequest.fail(function (jqXHR, textStatus, errorThrown) {
    if (textStatus === 'timeout') {
      console.log('The server is not responding')
    }
    if (textStatus === 'error') {
      console.log(errorThrown)
    }
  })
}

function addNewSite (siteName, siteDescription, dsuId) {
  console.log('Adding new site: ' + siteName + ' ' + siteDescription + ' ' + dsuId)

  $.ajax({
    url: 'http://localhost:3000/v1/api/sites',
    type: 'PUT',
    dataType: 'json',
    data: {
      'dsuId': dsuId,
      'name': siteName,
      'description': siteDescription },
    success: function (response) {
      console.log(response)
      const newRow = `
        <tr>
            <td>${response.id}</td>
            <td>${response.name}</td>
            <td>${response.description}</td>
        </tr>
      `
      $('tbody', `div[title="${dsuId}"]`).append(newRow)
      $('table').trigger('updateAll')
    },
    error: function (response) {
      alert(response.responseText)
    }
  })
}

function addNewDSU (dsuName, dsuDescription, dsuCert) {
  $.ajax({
    url: 'http://localhost:3000/v1/api/dsus',
    type: 'PUT',
    dataType: 'json',
    data: {
      'name': dsuName,
      'description': dsuDescription,
      'cert': dsuCert
    },
    success: function (response) {
      console.log(response)
      const dsuDiv = createDsuDiv(response, '')
      $(dsuDiv).insertAfter(previousDiv)
      previousDiv = `#dsu_${response.id}`
      // Activate tablesorter
      $(`#sitesTable_${response.id}`).tablesorter({ theme: 'default' })
      $('table').trigger('updateAll')
    },
    error: function (response) {
      alert(response.responseText)
    }
  })
}

$(document).ready(function () {
  // Hide the DSU section until clicked
  $('#landingArrowDiv').hide()
  $('#dsuDiv').hide()

  var getSitesRequest = $.get('http://127.0.0.1:3000/v1/api/sites')
  getSitesRequest.done(function (siteResult) {
    getDsus(siteResult)
  })
  getSitesRequest.fail(function (jqXHR, textStatus, errorThrown) {
    if (textStatus === 'timeout') {
      console.log('The server is not responding')
    }
    if (textStatus === 'error') {
      console.log(errorThrown)
    }
  })

  // Searchbar function hides and shows DSUs based on search
  $('#searchBar').on('input', function (e) {
    var input = $(this)
    var val = input.val()
    if (input.data('lastval') !== val) {
      input.data('lastval', val)
      $('.dsuDiv').each(function (i, obj) {
        if (!($('.dsuNameValue', this).is(`:contains("${val}")`) ||
                    $('.dsuDescriptionValue', this).is(`:contains("${val}")`) ||
                    $('.dsuCertValue', this).is(`:contains("${val}")`) ||
                    $(this).attr('id') === 'addDsuOuterDiv'
        )) {
          $(this).hide()
        } else {
          $(this).show()
        }
      })
    }
  })

  // Press the expand button on a DSU
  $('#dsuDiv').on('click', '.expandSpan', function (event) {
    const dsuDiv = this.parentNode.parentNode.id
    if ($(this).is(':contains("▲")')) {
      $(`#${dsuDiv}`).css('height', '60px')
      $(`#${dsuDiv}`).css('grid-template-rows', '35px 0px 20px 5px')
      $('.sitesDiv', `#${dsuDiv}`).css('visibility', 'hidden')
      $(this).text('▼')
    } else {
      $(`#${dsuDiv}`).css('height', '400px')
      $(`#${dsuDiv}`).css('grid-template-rows', '35px 340px 20px 5px')
      $('.sitesDiv', `#${dsuDiv}`).css('visibility', 'visible')
      $(this).text('▲')
    }
  })

  // Press the add site button
  $('#dsuDiv').on('click', '.tickSelector', function (event) {
    const siteName = this.parentNode.childNodes[1].value
    const siteDescription = this.parentNode.childNodes[3].value
    const dsuId = this.parentNode.parentNode.parentNode.parentNode.title
    addNewSite(siteName, siteDescription, dsuId)
  })

  // Press the add DSU button
  $('#dsuDiv').on('click', '#dsuSelector', function (event) {
    const dsuName = this.parentNode.childNodes[1].value
    const dsuDescription = this.parentNode.childNodes[3].value
    const dsuCert = this.parentNode.childNodes[5].value
    addNewDSU(dsuName, dsuDescription, dsuCert)
  })

  $('#landingArrowSpan').click(function () {
    $('#landingArrowDiv').hide()
    $('#dsuArrowDiv').show()
    $('#dsuDiv').hide()
    $('#landingDiv').show()
  })
  $('#dsuArrowSpan').click(function () {
    $('#dsuArrowDiv').hide()
    $('#landingArrowDiv').show()
    $('#dsuDiv').show()
    $('#landingDiv').hide()
  })
})
