async function setupDatabase (con) {
  console.log('Creating DB')
  await con.query('DROP DATABASE IF EXISTS powerDB')
  await con.query('CREATE DATABASE IF NOT EXISTS powerDB')
  await con.query('use powerDB')

  console.log('Creating Tables')
  await con.query('CREATE TABLE IF NOT EXISTS dsus (id INT NOT NULL AUTO_INCREMENT, description VARCHAR(255), PRIMARY KEY (id))')
  await con.query('CREATE TABLE IF NOT EXISTS site_power (id INT, power DECIMAL(13, 4), dsu_id INT, time_sent DATETIME, PRIMARY KEY (id), FOREIGN KEY (dsu_id) REFERENCES dsus(id))')
  await con.query('CREATE TABLE IF NOT EXISTS dsu_power (dsu_id INT, total_power DECIMAL(13, 4), time_aggregated DATETIME, PRIMARY KEY (dsu_id), FOREIGN KEY (dsu_id) REFERENCES dsus(id))')
  console.log('Populating Tables')

  const dsuNames = [
    ['Red DSU'],
    ['Green DSU'],
    ['Blue DSU'],
    ['Yellow DSU'],
    ['Red DSU'],
    ['Black DSU'],
    ['White DSU'],
    ['Grey DSU'],
    ['Purple DSU'],
    ['Pink DSU'],
    ['Indigo DSU'],
    ['Violet DSU'],
    ['Orange DSU']
  ]

  await con.query('INSERT INTO dsus (description) VALUES ?', [dsuNames])

  var dsuPowers = []
  for (var i = 0; i < dsuNames.length; i++) {
    dsuPowers.push([i + 1, 0])
  }
  await con.query('INSERT INTO dsu_power (dsu_id, total_power) VALUES ?', [dsuPowers])

  var sites = []
  for (i = 0; i < 100; i++) {
    sites.push([i, (Math.floor(Math.random() * dsuNames.length)) + 1])
  }
  return con.query('INSERT INTO site_power (id, dsu_id) VALUES ?', [sites])
}

exports.setupDatabase = setupDatabase
