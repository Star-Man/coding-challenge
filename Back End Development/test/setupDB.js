const expect = require('chai').expect
const { createDatabaseConnection } = require('../connectToDatabase')
const { setupDatabase } = require('../setupDatabase')

describe('Setup Database', function () {
  context('default', function () {
    it('should return sum of arguments', async function () {
      // Setup the Database
      const con = await createDatabaseConnection()
      await setupDatabase(con)

      // Get a list of the tables in SQL
      const [SQLtableList] = await con.query(`SHOW TABLES`)
      const expectedTableList = '[{"Tables_in_powerDB":"dsu_power"},{"Tables_in_powerDB":"dsus"},{"Tables_in_powerDB":"site_power"}]'
      // The tables should be what we expect
      expect(JSON.stringify(SQLtableList)).to.equal(expectedTableList)

      // Get a list of the dsus in SQL
      const [SQLdsuList] = await con.query(`SELECT * FROM dsus`)
      const expectedDsusList = '[{"id":1,"description":"Red DSU"},{"id":2,"description":"Green DSU"},{"id":3,"description":"Blue DSU"},{"id":4,"description":"Yellow DSU"},{"id":5,"description":"Red DSU"},{"id":6,"description":"Black DSU"},{"id":7,"description":"White DSU"},{"id":8,"description":"Grey DSU"},{"id":9,"description":"Purple DSU"},{"id":10,"description":"Pink DSU"},{"id":11,"description":"Indigo DSU"},{"id":12,"description":"Violet DSU"},{"id":13,"description":"Orange DSU"}]'
      expect(JSON.stringify(SQLdsuList)).to.equal(expectedDsusList)

      // Get a list of the dsu_power in SQL
      const [sqlDsuPowerList] = await con.query(`SELECT * FROM dsu_power`)
      const expectedDsuPowerList = '[{"dsu_id":1,"total_power":"0.0000","time_aggregated":null},{"dsu_id":2,"total_power":"0.0000","time_aggregated":null},{"dsu_id":3,"total_power":"0.0000","time_aggregated":null},{"dsu_id":4,"total_power":"0.0000","time_aggregated":null},{"dsu_id":5,"total_power":"0.0000","time_aggregated":null},{"dsu_id":6,"total_power":"0.0000","time_aggregated":null},{"dsu_id":7,"total_power":"0.0000","time_aggregated":null},{"dsu_id":8,"total_power":"0.0000","time_aggregated":null},{"dsu_id":9,"total_power":"0.0000","time_aggregated":null},{"dsu_id":10,"total_power":"0.0000","time_aggregated":null},{"dsu_id":11,"total_power":"0.0000","time_aggregated":null},{"dsu_id":12,"total_power":"0.0000","time_aggregated":null},{"dsu_id":13,"total_power":"0.0000","time_aggregated":null}]'
      expect(JSON.stringify(sqlDsuPowerList)).to.equal(expectedDsuPowerList)

      // Get a list of the site_power in SQL
      const [sqlSitePowerList] = await con.query(`SELECT * FROM site_power`)
      // Can't test if equal since the values are randomised, so we test size
      expect(sqlSitePowerList.length).to.equal(100)
    })
  })
})
