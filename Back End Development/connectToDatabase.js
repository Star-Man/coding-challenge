const mysql = require('mysql2/promise')

async function createDatabaseConnection () {

  // Exit if the user didn't set their mysql password/user
  if (process.env.MYSQLUSER === null || process.env.MYSQLUSER === undefined){
      console.log("The MYSQLUSER environment variable was not set")
      process.exit(1)
  }
  if (process.env.MYSQLPASSWORD === null || process.env.MYSQLPASSWORD === undefined){
      console.log("The MYSQLPASSWORD environment variable was not set")
      process.exit(1)
  }

  return mysql.createConnection({
    host: 'localhost',
    user: process.env.MYSQLUSER,
    password: process.env.MYSQLPASSWORD
  })
}

exports.createDatabaseConnection = createDatabaseConnection
