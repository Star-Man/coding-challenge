# Front End Development

This folder contains HTML/CSS/JS files and libraries that are
required to run the site.

## Running
To run, just open index.html in your browser.

This will display the website but to do anything, you should have the API server running at 127.0.0.1:3000


Tested browsers are firefox and chrome,
but due to the way firefox handles CORS, firefox may not work.

Tested OS was linux. Although this is a basic website and will run on any machine capable of running chrome/firefox.


The resolution I tested at was 2560 × 1440 so resolutions higher
than that probably won't look great.

## Info

This showcase attempts to follow all requirements, except in cases where I felt there was a better option in terms of usability.

For example, the menu that was specified is replaced by an arrow to a list of DSU's. Instead of a single table, each DSU has its own table.

Minimum supported resolution is 600*260.

The only specified features that are missing are the menu bar, which I felt was better replaced by a simple 2-page system, and the ability to add multiple forms to input at once, which I didn't add because I felt it was too cluttered and slightly less usable. Of course in a realistic suitation it's something to discuss with the client.

The libraries used are Jquery, and TableSorter.

## Media

Here is what I originally created as a mockup:


![](media/homePageMockup.png "Landing Page Mockup")
![](media/dsuPageMockup.png "DSU Page Mockup")


And here is what the website turned out as:

![](media/homePageFinal.png "Landing Page Final")
![](media/addNewSiteFinal.png "DSU Page Final")
![](media/sitesListFinal.png "DSU Page Final")
